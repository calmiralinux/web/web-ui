import { STRING_EMPTY } from './consts';

export const APP_URLS = {
  ROOT: STRING_EMPTY,
  PACKAGES: 'packages',
}
