import { defer, map, Observable } from 'rxjs';

export function prepare<T>(callback: () => void): (source: Observable<T>) => Observable<T> {
  return (source: Observable<T>): Observable<T> =>
    defer(() => {
      callback();
      return source;
    });
}
export const mapToVoid = () => function <T>(source: Observable<T>) {
  return source.pipe(
    map(() => (void 0)),
  );
};
