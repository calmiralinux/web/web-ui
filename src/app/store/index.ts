import { inject, Injectable, isDevMode } from '@angular/core';
import { createStore, enableElfProdMode, select, withProps } from '@ngneat/elf';
import { devTools } from '@ngneat/elf-devtools';
import { Observable, tap } from 'rxjs';
import { Api, DEFAULT_BRANCH } from '../api';
import { PortMetaData } from '../api/models';

import { mapToVoid, prepare } from '../utils';

isDevMode() ? devTools(): enableElfProdMode();

export interface StoreItem<T> {
  item: T| null,
  isLoading: boolean,
  error: string | null;
}

export interface PortsList {
  ports: StoreItem<PortMetaData[]>
}

export interface Search {
  search: string;
}

export interface Category {
  category: string;
}

export interface Categories {
  categories: string[];
}
export const DEFAULT_ITEM = { item: null, isLoading: false, error: null }

export const calmiraStore = createStore(
  { name: 'CalmiraStore' },
  withProps<PortsList>({ ports: DEFAULT_ITEM }),
  withProps<Search>({ search: '' }),
  withProps<Category>({ category: '' }),
  withProps<Categories>({ categories: [] }),
);

export const setSearch = (search: string)  => calmiraStore.update((state) => ({
  ...state,
  search:search
}));

export const setCategory = (category: string)  => calmiraStore.update((state) => ({
  ...state,
  category:category
}));

export const setCategories = (categories: string[])  => calmiraStore.update((state) => ({
  ...state,
  categories: categories
}));

export const startLoadingPorts = () => calmiraStore.update((state) => ({
  ...state,
  ports: {
    ...state.ports,
    isLoading: true,
    error: null
  }
}));

const searchItems = (ports: PortMetaData[], search: string, category?: string) => {
  if (search)  ports = ports.filter((item) => item.package.name.includes(search))
  if (category)  ports = ports.filter((item) => item.package.fullName.startsWith(category!))

  return ports;
}

export const setPorts = (ports: PortMetaData[], search: string, category?: string) => calmiraStore.update((state) => ({
  ...state,
  ports: {
    item: searchItems(ports, search, category),
    isLoading: false,
    error: null
  }
}));

export const portsIsLoading = calmiraStore.pipe(select((state) => state.ports.isLoading));

export const ports = calmiraStore.pipe(
  select((state) => state.ports.item));

export const search = calmiraStore.pipe(
  select((state) => state.search));

export const category = calmiraStore.pipe(
  select((state) => state.category));

export const categories = calmiraStore.pipe(
  select((state) => state.categories));

@Injectable({ providedIn: 'root'})
export class CalmiraStoreService {
  private readonly api = inject(Api);

  public loadPortList(
    branch: string = DEFAULT_BRANCH,
    search: string,
    category?: string
  ): Observable<void> {
    return this.api.getPortList(branch).pipe(
      prepare(() => startLoadingPorts()),
      tap((response) => setPorts(response, search, category)),
      mapToVoid(),
    );
  }

  public loadMetadata(branch: string = DEFAULT_BRANCH,) {
    return this.api.getMetaData(branch).pipe(
      tap((response) => setCategories(response.port_sys.categories ?? []))
    )
  }
}
