import { ChangeDetectionStrategy, Component, DestroyRef, inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { LetDirective } from '@ngrx/component';
import { TuiLoaderModule } from '@taiga-ui/core';
import { combineLatest, distinctUntilChanged, switchMap, tap } from 'rxjs';
import { DEFAULT_BRANCH } from '../../../../api';
import { CalmiraStoreService, category, ports, portsIsLoading, search } from '../../../../store';
import { PortsListFilterComponent } from './components/ports-list-filter/ports-list-filter.component';
import { PortsListComponent } from './components/ports-list/ports-list.component';

@Component({
  selector: 'calmira-web-packages-page',
  standalone: true,
  imports: [ CommonModule, LetDirective, TuiLoaderModule, PortsListComponent, PortsListFilterComponent ],
  templateUrl: './packages-page.component.html',
  styleUrls: [ './packages-page.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PackagesPageComponent implements OnInit {
  public readonly isLoading = portsIsLoading;
  public readonly ports = ports;

  private readonly store = inject(CalmiraStoreService);
  private readonly destroyRef = inject(DestroyRef);
  private readonly searchResult = search;
  private readonly categoryResult = category;

  public ngOnInit(): void {
    this.loadPorts();
  }

  private loadPorts(): void {
    combineLatest([
      this.searchResult,
      this.categoryResult,
    ]).pipe(
      distinctUntilChanged(),
      switchMap(([ search, category ]) =>
        this.store.loadMetadata(DEFAULT_BRANCH).pipe(
          switchMap(() => this.store.loadPortList(DEFAULT_BRANCH, search, category)))
      ),
      takeUntilDestroyed(this.destroyRef)
    ).subscribe();

    this.searchResult.pipe(
      distinctUntilChanged(),
      switchMap((search) => this.store.loadPortList(DEFAULT_BRANCH, search)),
      takeUntilDestroyed(this.destroyRef)
    ).subscribe()
  }
}
