import { ChangeDetectionStrategy, Component, DestroyRef, inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { LetDirective } from '@ngrx/component';
import { tuiPure } from '@taiga-ui/cdk';
import { TuiTextfieldControllerModule } from '@taiga-ui/core';
import { TuiComboBoxModule, TuiDataListWrapperModule, TuiInputModule, TuiIslandModule } from '@taiga-ui/kit';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs';
import { categories, category, search, setCategory, setSearch } from '../../../../../../store';
@Component({
  selector: 'calmira-web-ports-list-filter',
  standalone: true,
  imports: [ CommonModule, TuiIslandModule, TuiInputModule, ReactiveFormsModule, TuiTextfieldControllerModule, LetDirective, TuiComboBoxModule, TuiDataListWrapperModule ],
  templateUrl: './ports-list-filter.component.html',
  styleUrls: [ './ports-list-filter.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PortsListFilterComponent implements OnInit {
  public readonly searchControl = new FormControl('');
  public readonly categoriesControl = new FormControl('');

  private readonly destroyRef = inject(DestroyRef);

  private readonly searchResult = search;
  private readonly categoryResult = category;


  @tuiPure
  public get categories() {
    return categories
  }

  public ngOnInit(): void {
    this.searchResult.pipe(
      distinctUntilChanged(),
      tap((search) => this.searchControl.patchValue(search, { emitEvent: false, onlySelf: true })),
      takeUntilDestroyed(this.destroyRef),
    ).subscribe()

    this.categoryResult.pipe(
      distinctUntilChanged(),
      tap((category) => this.categoriesControl.patchValue(category, { emitEvent: false, onlySelf: true })),
      takeUntilDestroyed(this.destroyRef),
    ).subscribe()

    this.searchControl.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(200),
      tap((search) => setSearch(<string>search)),
      takeUntilDestroyed(this.destroyRef),
    ).subscribe()

    this.categoriesControl.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(200),
      tap((category) => setCategory(<string>category)),
      takeUntilDestroyed(this.destroyRef),
    ).subscribe()
  }
}
