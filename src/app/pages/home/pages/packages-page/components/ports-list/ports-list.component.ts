import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TuiTableModule } from '@taiga-ui/addon-table';
import { TuiButtonModule } from '@taiga-ui/core';
import { PortMetaData } from '../../../../../../api/models';

@Component({
  selector: 'calmira-web-ports-list',
  standalone: true,
  imports: [ CommonModule, TuiTableModule, TuiButtonModule ],
  templateUrl: './ports-list.component.html',
  styleUrls: ['./ports-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PortsListComponent {
  @Input()
  public ports: PortMetaData[] = [];

  public trackByItem (index: number, item: PortMetaData)  {
    return item.package.name;
  }

  public downloadPort(url: string): void {
    location.href = url;
  }
}
