import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { APP_URLS } from '../../app.urls';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path: APP_URLS.ROOT,
    component: LayoutComponent,
    children: [
      {
        path: APP_URLS.ROOT,
        pathMatch: 'full',
        redirectTo: APP_URLS.PACKAGES,
      },
      {
        path: APP_URLS.ROOT,
        loadComponent: async () =>
          (await import('./pages/home-page/home-page.component'))
            .HomePageComponent,
      },
      {
        path: APP_URLS.PACKAGES,
        loadComponent: async () =>
            (await import('./pages/packages-page/packages-page.component'))
                .PackagesPageComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
