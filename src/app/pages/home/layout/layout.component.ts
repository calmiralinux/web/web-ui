import { NgOptimizedImage } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { TuiButtonModule, TuiSvgModule } from '@taiga-ui/core';
import { TuiIslandModule, TuiLazyLoadingModule } from '@taiga-ui/kit';

@Component({
  selector: 'calmira-web-layout',
  standalone: true,
  templateUrl: './layout.component.html',
  imports: [
    RouterOutlet,
    NgOptimizedImage,
    TuiButtonModule,
    TuiSvgModule,
    RouterLink,
    RouterLinkActive,
    TuiIslandModule,
    TuiLazyLoadingModule
  ],
  styleUrls: [ './layout.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutComponent {
}
