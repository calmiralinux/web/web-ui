import { NgDompurifySanitizer } from '@tinkoff/ng-dompurify';
import { TuiRootModule, TuiDialogModule, TuiAlertModule, TUI_SANITIZER } from '@taiga-ui/core';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'calmira-web-root',
  standalone: true,
  imports: [ CommonModule, RouterOutlet, TuiRootModule, TuiDialogModule, TuiAlertModule ],
  template: `
    <tui-root>
      <router-outlet></router-outlet>
      <ng-container ngProjectAs="tuiOverContent"></ng-container>
      <ng-container ngProjectAs="tuiOverDialogs"></ng-container>
      <ng-container ngProjectAs="tuiOverAlerts"></ng-container>
      <ng-container ngProjectAs="tuiOverPortals"></ng-container>
      <ng-container ngProjectAs="tuiOverHints"></ng-container>
    </tui-root>
  `,
  providers: [{provide: TUI_SANITIZER, useClass: NgDompurifySanitizer} ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
}
