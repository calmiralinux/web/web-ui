export interface PortItemMetaDataBase {
  name: string;
  version: string;
  modified: Date;
}

export interface Metadata {
  port_sys: {
    categories: string[],
    ports: PortItemMetaDataBase[]
  },
  system: {
    releases: string[]
  }
}

export interface PortMetaData {
  package: {
    name: string;
    version: string;
    description: string;
    maintainers: string[];
    releases: string[];
    priority: string;
    usage: number;
    upgrade_mode: string;
    build_time: number;
    branch: string;
    fullName: string;
  },
  deps: {
    required: []
  },
  port: {
    url: string;
    file: string;
    md5: string;
    sha256: string;
  }
}

