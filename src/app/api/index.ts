import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Metadata, PortMetaData } from './models';

export const DEFAULT_BRANCH = 'ports_v2.0';

@Injectable({ providedIn: 'root'})
export class Api {
  constructor(private readonly http: HttpClient) {}

  public getBranches(): Observable<string[]> {
    return of([DEFAULT_BRANCH]);
  }

  public getMetaData(branch: string = DEFAULT_BRANCH) : Observable<Metadata>{
    return this.http.get<Metadata>(`api/v1/ports/metadata/${branch}`);
  }

  public getPortMetaData(branch: string = DEFAULT_BRANCH, port: string) : Observable<PortMetaData>{
    return this.http.get<PortMetaData>(`api/v1/ports/metadata/${branch}/${port}`);
  }

  public getPortMetaDataInstall(branch: string = DEFAULT_BRANCH, port: string) : Observable<string>{
    return this.http.get<string>(`api/v1/ports/metadata/${branch}/${port}/install`);
  }

  public getPortMetaDataReadme(branch: string = DEFAULT_BRANCH, port: string) : Observable<string>{
    return this.http.get<string>(`api/v1/ports/metadata/${branch}/${port}/readme`);
  }

  public getPortList(branch: string = DEFAULT_BRANCH) : Observable<PortMetaData[]>{
    return this.http.get<PortMetaData[]>(`api/v1/ports/list/${branch}`);
  }
}
