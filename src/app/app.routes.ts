import { Routes } from '@angular/router';
import { APP_URLS } from './app.urls';

export const routes: Routes = [
  {
    path: APP_URLS.ROOT,
    loadChildren: async () =>
      (await import('./pages/home/home.module')).HomeModule,
  }
];
